
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.page1, name='page1'),
    path('send-message/', views.page2, name='page2'),
]
