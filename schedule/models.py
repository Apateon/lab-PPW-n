from django.db import models

# Create your models here.
class Schedule(models.Model):
    activity_name = models.CharField(max_length = 100)
    date_time = models.DateTimeField('Activity Time')
    activity_place = models.CharField(max_length = 100)
    activity_category = models.CharField(max_length = 100)