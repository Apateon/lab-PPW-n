from django.shortcuts import render
from django.http import HttpResponse

from .models import Schedule

# Create your views here.
def index(request):
    return HttpResponse("Hi")
    #return render(request, 'page1.html')

def data(request):
    schedule_list = Schedule.objects.order_by('-date_time')[:]
    context = {'schedule_list' : schedule_list}
    return render(request, 'schedule/index.html', context)