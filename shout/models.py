from django.db import models

# Create your models here.
class Shout(models.Model):
    date_time = models.DateTimeField(auto_now_add=True)
    shout = models.TextField(max_length=300)