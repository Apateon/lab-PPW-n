from django.test import TestCase

from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys

from django.test import Client
from django.urls import resolve
from .views import index, profile
from .models import Shout
from .forms import ShoutForm

# Create your tests here.
class Shout_Test(TestCase):
    
    def test_shout_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_shout_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_shout_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_shout(self):
        new_shout = Shout.objects.create(shout="lorem ipsum")
        count_shout = Shout.objects.all().count()
        self.assertEqual(count_shout, 1)

    def test_ShoutForm_valid(self):
        form = ShoutForm(data={'shout': "lorem ipsum"})
        self.assertTrue(form.is_valid())

    def test_ShoutForm_invalid(self):
        form = ShoutForm(data={'shout': ""})
        self.assertFalse(form.is_valid())

    def test_profile_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code,200)

    def test_profile_using_profile(self):
        response = Client().get('/profile')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_using_profile_func(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

# class FunctionTest(unittest.TestCase):
#     def setUp(self):
#         self.browser = webdriver.Firefox()
#         super(FunctionTest,self).setUp()
        
#     def tearDown(self):
#         #self.browser.implicitly_wait(3)
#         #time.sleep(5)
#         self.browser.quit()

#     def test_can_post_and_show(self):
#         selenium = self.browser
#         selenium.get('https://ppw-c-ol-newt.herokuapp.com/')
#         form = selenium.find_element_by_id('id_shout')
#         submit = selenium.find_element_by_id('submit')
#         text = 'Coba Coba'
#         form.send_keys(text)
#         #time.sleep(3)
#         submit.send_keys(Keys.RETURN)
#         selenium.refresh()
#         result = selenium.find_elements_by_id('shouts')
#         self.assertIn(text, result[0].text)
#         #self.assertIn(text, result)
