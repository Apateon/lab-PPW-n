from django import forms
from .models import Shout

class ShoutForm(forms.Form):
    shout = forms.CharField(label="", max_length=300) # widget=forms.Textarea,

    def clean(self):
        cleaned_data = super().clean()
        shout = cleaned_data.get("shout")
