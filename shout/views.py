from django.shortcuts import render, redirect

from .forms import ShoutForm
from .models import Shout

# Create your views here.
def index(request):
    shout_list = Shout.objects.order_by('-date_time')[:]
    form = ShoutForm(request.POST or None)
    context = {
        'shout_list' : shout_list,
        'form' : form
        }
    if(request.method == "POST" and form.is_valid()):
        shout = Shout(shout = request.POST.get("shout"))
        shout.full_clean()
        shout.save()
        return redirect('/')
    else:
        return render(request, 'index.html', context)
    
def profile(request):
    return render(request, 'profile.html')